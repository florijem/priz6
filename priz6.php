<?php
// priz6
// Copyright or © or Copr. Florijem, Florijem@ideovif.net, 20-08-2018.
// This library is a firewall in PHP to block permanently or temporary the  
// unwanted.
// This software is governed by the CeCILL-B license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL-B
// license as circulated by CEA, CNRS and INRIA at the following URL
// http://www.cecill.info. 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL-B license and that you accept its terms. -->

class Priz6
{ private $prison;
  private $path_db =
    './priz6_db.php';
  private $maxtry =
    3;
  private $bantime =
    3960;
  
  function __construct ($path = null)
  { if ( isset($path) ) { $this->path_db = $path; }
    
    if ( file_exists($this->path_db) )
    { include $this->path_db;
      $this->prison =
        $priz6_db;
    } else
    { $this->prison =
        array('banever' => array(), 'anomaly' => array(), 'ban' => array());
      file_put_contents
        ( $this->path_db,
          ( "<?php\n\$priz6_db =\n"
          . var_export($this->prison, true)
          . ";\n?>" 
          )
        );
    }
  }
  
  private function write ()
  { return 
      ( is_int
          ( file_put_contents
              ( $this->path_db,
                ( "<?php\n\$priz6_db =\n"
                . var_export($this->prison, true)
                . ";\n?>" 
                )
              )
          )
      ? true
      : false
      );
  }

  function clean_prison ()
  { $this->prison =
      array('banever' => array(), 'anomaly' => array(), 'ban' => array());
    return $this->write();
  }
  function clean_anomaly ()
  { $this->prison['anomaly'] =
      array();
    return $this->write();
  }
  function clean_ban ()
  { $this->prison['ban'] =
      array();
    return $this->write();
  }
  function clean_banever ()
  { $this->prison['banever'] =
      array();
    return $this->write();
  }

  function set_maxtry ( $try_number )
  { $this->maxtry =
      $try_number;
    return $this->maxtry;
  }
  function set_bantime ( $seconds )
  { $this->bantime =
      $seconds;
    return $this->bantime;
  }

  private function unset_anomaly ( $name )
  { unset($this->prison['anomaly'][$name]);
    return true;
  }
  private function unset_ban ( $name )
  { unset($this->prison['ban'][$name]);
    return true;
  }
  private function unset_banever ( $name )
  { unset($this->prison['banever'][$name]);
    return true;
  }

  function is_banever ( $name )
  { return array_key_exists($name, $this->prison['banever']);
  }
  function is_ban ( $name )
  { if ( !array_key_exists($name, $this->prison['ban']) ) { return false; }
    if ( $this->prison['ban'][$name] > time() ) { return true; }
    $this->unset_ban($name);
    $this->write();
    return false;
  }

  function banever ( $name, $code = 0 )
  { if ( $this->is_banever($name) ) { return false; }
    if ( $this->is_ban($name) ) { $this->unset_ban($name); }
    $this->prison['banever'][$name] =
      $code;
    return $this->write();
  }
  private function ban ( $name, $anomalies )
  { $this->prison['ban'][$name]['time'] =
      ( time() + $this->bantime );
    $this->prison['ban'][$name]['anomalies'] =
      $anomalies;
    return $this->write();
  }
  function add_anomaly( $name, $code = 0 )
  { if ( $this->is_ban($name) ) { return false; }
    if ( $this->is_banever($name) ) { return false; }
    
    if ( array_key_exists($name, $this->prison['anomaly']) )
    { $this->prison['anomaly'][$name][] =
        $code;
    } else
    { $this->prison['anomaly'][$name] =
        array($code);
    }

    if ( count($this->prison['anomaly'][$name]) >= $this->maxtry )
    { if ( count(array_unique($this->prison['anomaly'][$name])) === 1 )
      { return $this->banever($name, $code);
      } else
      { return $this->ban($name, $this->prison['anomaly'][$name]);
      }
    }

    return true;
  }
}
?>