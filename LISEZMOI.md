priz6
=====

Se prononce `prison`.

Il s'agit d'un pare-feu en PHP pour bloquer les indésirables de façon  
permanente ou temporaire.

Usage
-----

La version `7.0.0` de PHP est requise.

Inclure `priz6` dans un source PHP avec l'une des possibilités  
suivantes :

```php
include '/chemin/priz6.php';
include_once '/chemin/priz6.php';
require '/chemin/priz6.php';
require_once '/chemin/priz6.php';
```

Créer une instance de `priz6` :

```php
$p = new Priz6();
```

Pour protéger la base de données sur le serveur, il est préférable de la  
mettre en dehors de la racine d'un site web.  
Le chemin par défaut de la base de donnée est `./priz6_db.php`.  
Pour le changer, instancier `priz6` avec le chemin désiré :  

```php
$p = new Priz6('/chemin/nom.php');
```

Le nombre d'anomalie autorisée par défaut avant le banissement est de `3`.  
Pour le changer, paramétrer avec la méthode `set_maxtry` :

```php
$p->set_maxtry(2);
```

Le nombre de seconde de banissement par défaut est de `3960` (66 minutes).  
Pour le changer, paramétrer avec la méthode `set_bantime` :

```php
$p->set_bantime(600);
```

Documentation
-------------

- clean_prison :

    ```php
    // $p->clean_prison () -> boolean
    $p->clean_prison ();
    ```

- clean_anomaly :

    ```php
    // $p->clean_anomaly () -> boolean
    $p->clean_anomaly ();
    ```

- clean_ban :

    ```php
    // $p->clean_ban () -> boolean
    $p->clean_ban ();
    ```

- clean_banever :

    ```php
    // $p->clean_banever () -> boolean
    $p->clean_banever ();
    ```

- set_maxtry :

    ```php
    // $p->set_maxtry ( integer ) -> integer
    $p->set_maxtry ( 3 );
    ```

- set_bantime :

    ```php
    // $p->set_bantime ( integer ) -> integer
    $p->set_bantime ( 3960 );
    ```

- is_banever :

    ```php
    // $p->is_banever ( string ) -> boolean
    $p->is_banever ( $name );
    ```

- is_ban :

    ```php
    // $p->is_ban ( string ) -> boolean
    $p->is_ban ( $name );
    ```

- banever :

    ```php
    // $p->banever ( string [ , integer = 0 ] ) -> boolean
    $p->banever ( $name );
    $p->banever ( $name , $code );
    ```

- add_anomaly :

    ```php
    // $p->add_anomaly ( string [ , integer = 0 ] ) -> boolean
    $p->add_anomaly ( $name );
    $p->add_anomaly ( $name , $code );
    ```

Licence
-------

Copyright ou © ou Copr. Florijem, <florijem@ideovif.net>, 20-08-2018. 

Cette bibliothèque est un pare-feu en PHP pour bloquer les indésirables  
de façon permanente ou temporaire.

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et  
respectant les principes de diffusion des logiciels libres. Vous pouvez  
utiliser, modifier et/ou redistribuer ce programme sous les conditions  
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA  
sur le site <http://www.cecill.info>.

En contrepartie de l'accessibilité au code source et des droits de copie,  
de modification et de redistribution accordés par cette licence, il n'est  
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,  
seule une responsabilité restreinte pèse sur l'auteur du programme,  le  
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques  
associés au chargement,  à l'utilisation,  à la modification et/ou au  
développement et à la reproduction du logiciel par l'utilisateur étant  
donné sa spécificité de logiciel libre, qui peut le rendre complexe à  
manipuler et qui le réserve donc à des développeurs et des professionnels  
avertis possédant  des  connaissances  informatiques approfondies.  Les  
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du  
logiciel à leurs besoins dans des conditions permettant d'assurer la  
sécurité de leurs systèmes et ou de leurs données et, plus généralement,  
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez  
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les  
termes.
