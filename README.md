priz6
=====

Is pronounced `prison`.

This is a firewall in PHP to block permanently or temporary the  
unwanted.

Usage
-----

The PHP version `7.0.0` is required.

Include `priz6` in a PHP source with one of the following  
possibilities :

```php
include '/path/priz6.php';
include_once '/path/priz6.php';
require '/path/priz6.php';
require_once '/path/priz6.php';
```

Create an instance of `priz6` :

```php
$p = new Priz6();
```

To protect the database on the server, it is best to put it outside the  
root of a website.  
The default path of the database is `./priz6_db.php`.  
To change it, instance `priz6` with the desirated path :

```php
$p = new Priz6('/path/name.php');
```

The default number of authorized anomaly before ban is `3`.  
To change it, set with the method `set_maxtry` :

```php
$p->set_maxtry(2);
```

The default number of ban time is `3960`.  
To change it, set with the method `set_bantime` :

```php
$p->set_bantime(600);
```

Documentation
-------------

- clean_prison :

    ```php
    // $p->clean_prison () -> boolean
    $p->clean_prison ();
    ```

- clean_anomaly :

    ```php
    // $p->clean_anomaly () -> boolean
    $p->clean_anomaly ();
    ```

- clean_ban :

    ```php
    // $p->clean_ban () -> boolean
    $p->clean_ban ();
    ```

- clean_banever :

    ```php
    // $p->clean_banever () -> boolean
    $p->clean_banever ();
    ```

- set_maxtry :

    ```php
    // $p->set_maxtry ( integer ) -> integer
    $p->set_maxtry ( 3 );
    ```

- set_bantime :

    ```php
    // $p->set_bantime ( integer ) -> integer
    $p->set_bantime ( 3960 );
    ```

- is_banever :

    ```php
    // $p->is_banever ( string ) -> boolean
    $p->is_banever ( $name );
    ```

- is_ban :

    ```php
    // $p->is_ban ( string ) -> boolean
    $p->is_ban ( $name );
    ```

- banever :

    ```php
    // $p->banever ( string [ , integer = 0 ] ) -> boolean
    $p->banever ( $name );
    $p->banever ( $name , $code );
    ```

- add_anomaly :

    ```php
    // $p->add_anomaly ( string [ , integer = 0 ] ) -> boolean
    $p->add_anomaly ( $name );
    $p->add_anomaly ( $name , $code );
    ```

License
-------

Copyright or © or Copr. Florijem, <florijem@ideovif.net>, 20-08-2018. 

This library is a firewall in PHP to block permanently or temporary the  
unwanted.

This software is governed by the CeCILL-B license under French law and  
abiding by the rules of distribution of free software.  You can  use,  
modify and/ or redistribute the software under the terms of the CeCILL-B  
license as circulated by CEA, CNRS and INRIA at the following URL  
<http://www.cecill.info>.

As a counterpart to the access to the source code and  rights to copy,  
modify and redistribute granted by the license, users are provided only  
with a limited warranty  and the software's author,  the holder of the  
economic rights,  and the successive licensors  have only  limited  
liability.

In this respect, the user's attention is drawn to the risks associated  
with loading,  using,  modifying and/or developing or reproducing the  
software by the user in light of its specific status of free software,  
that may mean  that it is complicated to manipulate,  and  that  also  
therefore means  that it is reserved for developers  and  experienced  
professionals having in-depth computer knowledge. Users are therefore  
encouraged to load and test the software's suitability as regards their  
requirements in conditions enabling the security of their systems and/or  
data to be ensured and,  more generally, to use and operate it in the  
same conditions as regards security.

The fact that you are presently reading this means that you have had  
knowledge of the CeCILL-B license and that you accept its terms.
